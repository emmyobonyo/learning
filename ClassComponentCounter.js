import React, { Component } from 'react';

class ClassCounter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0,
    };
  }

  incrementCount = () => {
    this.setState((prevState) => ({ count: prevState.count + 1 }));
  }

  render() {
    const { count } = this.state;
    return (
      <div>
        <button onClick={this.incrementCount} type="button">
          Count
          { count }
        </button>
      </div>
    );
  }
}

export default ClassCounter;


import React, { useState } from 'react';

function ClassCounter() {
  const [count, setCount] = useState(0);
  return (
    <div>
      <button type="button" onClick={() => setCount(count )}>
        Count
        { count }
      </button>
    </div>
  );
}

export default ClassCounter;

import React, { useState } from 'react';

function ClassCounter() {
  const initialCount = 0;
  const [count, setCount] = useState(initialCount);
  return (
    <div>
      Count:
      { count }
      <button type="button" onClick={() => setCount(initialCount)}>Reset</button>
      <button type="button" onClick={() => setCount(count + 1)}>Increment</button>
      <button type="button" onClick={() => setCount(count - 1)}>Decrement</button>
    </div>
  );
}

import React, { useState } from 'react';

//Passing previous funcitin to get current functionguit
function ClassCounter() {
  const initialCount = 0;
  const [count, setCount] = useState(initialCount);
  return (
    <div>
      Count:
      { count }
      <button type="button" onClick={() => setCount(initialCount)}>Reset</button>
      <button type="button" onClick={() => setCount((previousCount) => previousCount + 1)}>Increment</button>
      <button type="button" onClick={() => setCount((previousCount) => previousCount - 1)}>Decrement</button>
    </div>
  );
}

export default ClassCounter;


export default ClassCounter;

import React, { useState } from 'react';

function ClassCounter() {
  const [name, setName] = useState({ firstName: '', lastName: '' });
  return (
    <div>
      <form>
        <input
          type="text"
          value={name.firstName}
          onChange={(e) => setName({ ...name, firstName: e.target.value })}
        />
        <input
          type="text"
          value={name.lastName}
          onChange={(e) => setName({
            ...name,
            lastName: e.target.value,
          })}
        />
        <h2>
          Your first name is -
          { name.firstName }
        </h2>
        <h2>
          Your last name is -
          { name.lastName }
        </h2>
        <h2>{JSON.stringify(name)}</h2>
      </form>
    </div>
  );
}

export default ClassCounter;

import React, { useState } from 'react';

function ClassCounter() {
  const [items, setItems] = useState([]);

  const addItem = () => {
    setItems([...items, {
      id: items.length,
      value: Math.floor(Math.random() * 10) + 1,
    }]);
  };
  return (
    <div>
      <button type="button" onClick={addItem}>Add a Number</button>
      <ul>
        {
          items.map((item) => (
            <li key={item.id}>{item.value}</li>
          ))
        }
      </ul>
    </div>
  );
}

export default ClassCounter;

// When dealing with objects and arrays, always make sure
// to spread your state variable and then call the setter function.
