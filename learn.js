// * Callbacks

function display(value) {
  document.getElementById('demo').innerHTML = value;
}

function newCalculator(num1, num2, display) {
  let sum = num1 + num2;
  display(sum);
}

newCalculator(5, 5, display)

function displayOne(value) {
  document.getElementById('demo1').innerHTML = value;
}

function getFile (callback) {
  let req = new XMLHttpRequest();
  req.open('GET', 'car.html');
  req.onload = function() {
    if (req.status == 200) {
      callback(this.responseText);
    } else {
      callback('Error' + req.status);
    }
  }
  req.send();
}

getFile(displayOne);