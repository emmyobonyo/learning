const { default: axios } = require("axios");

const functions = {
  checkValue: (x) => x,
  createUser: () => {
    const user = { firstName: 'Emmanuel' };
    user['lastName'] = 'Obonyo';
    return user;
  },
  lessThanOneK: () => {
    const load = 999.9
    return load;
  },
  fetchUser: () => axios
    .get('https://jsonplaceholder.typicode.com/users/1')
    .then(res => res.data)
    .catch( err => 'error')
}

module.exports = functions;