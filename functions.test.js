const { expect } = require('@jest/globals')
const functions = require('./functions')

test('should return a falsy value', () => {
  expect(functions.checkValue(0)).toBeFalsy();
})

test('user should be Emmanuel Obonyo object', () => {
  expect(functions.createUser()).toEqual({firstName: 'Emmanuel', lastName: 'Obonyo'});
})

test ('should be less than 1000', () => {
  expect(functions.lessThanOneK()).toBeLessThan(1000)
})

test('there is no i in team', () => {
  expect('teami').not.toMatch(/I/);
})

test('the array should contain Admin', () => {
  const usernames = ['not-admin', 'former-admin', 'employee', 'admin'];
  expect(usernames).toContain('admin')
})

test('user fetched data should be Leanne Graham', () => {
  return functions.fetchUser()
    .then(data => {
      expect(data.name).toBe('Leanne Graham')
    })
})

test('user fetched data should be hildegard.org', async () => {
  const data = await functions.fetchUser()
  expect(data.website).toBe('hildegard.org')
})
