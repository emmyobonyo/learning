const ul = document.getElementById('list-items');
const input = document.getElementsByTagName('input');
// const paragraph = document.getElementsByName('p');

HTMLCollection.prototype.forEach = Array.prototype.forEach;
NodeList.prototype.forEach = Array.prototype.forEach;

for (let i = 0; i<3; i++) {
  const li = document.createElement('li');
  li.id = i;
  const checkbox = document.createElement('input');
  checkbox.type = 'checkbox';
  checkbox.id = i;
  const p = document.createElement('p');
  p.innerHTML = `This is paragraph ${i}`;
  p.id = i;
  ul.appendChild(li);
  li.appendChild(checkbox);
  li.appendChild(p);
}

function hide (evt) {
  const target = evt.target;
  const li = target.parentElement;
  if (target.checked) {
    li.style.textDecoration = 'line-through';
  }
  else {
    li.style.textDecoration = 'none';
  }
}

input.forEach((input) => {
  input.addEventListener('click', hide, false);
});